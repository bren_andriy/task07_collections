package com.bren;

public class Application {
    public static void main(String[] args) {
        BinaryTreeMap<Integer,String> binaryTreeMap = new BinaryTreeMap<>();
        binaryTreeMap.put(1,"Andrii");
        binaryTreeMap.put(2,"Attractive");
        binaryTreeMap.put(3,"Student");
        binaryTreeMap.put(4,"Interview");
        for(BinaryTreeMap.Entry<Integer, String> entry: binaryTreeMap.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }
}
